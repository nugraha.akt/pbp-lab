# Jawaban Tugas 4

## 1. Apakah perbedaan antara JSON dan XML?

Perbedaan yang paling utama dari JavaScript Object Notation dengan eXtensible Markup Language terletak pada definisi dari keduanya sendiri. JSON merupakan salah satu format yang ditulis dalam JS. Sementara, XML merupakan bahasa markup dan menggunakan tag untuk membuat suatu elemen. Sehingga, JSON terkesan ringan dan ringkas daripada XML. Namun, kegunaan XML akan berada jauh diatas JSON karena XML memiliki kompleksitas yang lebih tinggi dan lebih cocok untuk digunakan dalam skala project skala besar.


## 2. Apakah perbedaan antara HTML dan XML?

Perbedaan yang paling utama dari HyperText Markup Language dengan eXtensible Markup Language terletak pada pengunaan dari keduanya sendiri. HTML dapat kita gunakan untuk menampilkan data, sementara XML kita gunakan untuk menyimpan suatu data. Walaupun mereka sama-sama menggunakan tag, tapi pengunaan tag didalamnya sangat berbeda. Jika di HTML kita menggunakan tag yang telah tersedia, di XML kita sendiri yang mendefinisikan tag sendiri. Sehingga, dapat kita simpulkan bahwa XML bagian dari menyimpan data, sementara HTML betugas untuk menampilkannya.