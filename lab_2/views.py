from django.http.response import HttpResponse
from django.core import serializers
from django.shortcuts import render
from .models import Note

def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab2.html', response)

def xml(request):
    response = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(response, content_type='application/xml')

def json(request):
    response = serializers.serialize('json', Note.objects.all())
    return HttpResponse(response, content_type='application/json')