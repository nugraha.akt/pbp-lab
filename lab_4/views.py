from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from lab_4.forms import NoteForm
from .models import Note

def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    if request.method == "POST":
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
    return render(request, 'lab4_form.html')

def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)