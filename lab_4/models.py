from django.db import models

class Note(models.Model):
    to = models.CharField(max_length=10)
    froms = models.CharField(max_length=10)
    title = models.CharField(max_length=100)
    message = models.TextField()

    def __str__(self):
        return f"{self.to} - {self.title}"