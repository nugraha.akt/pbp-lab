from django.forms import ModelForm
from django import forms
from .models import Friend

class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields=['name', 'npm', 'dob']


    widgets = {
            'dob': forms.DateInput(
                format=('%d/%m/%Y'),
                attrs={'class': 'form-control', 
                       'placeholder': 'Select a date',
                       # 'type': 'date'  # <--- IF I REMOVE THIS LINE, THE INITIAL VALUE IS DISPLAYED
                      }),
    }