from django.db import models
from django import forms

class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.CharField(max_length=10)
    dob = models.DateField()

    widgets = {
            'dob': forms.DateInput(
                format=('%d/%m/%Y'),
                attrs={'class': 'form-control', 
                       'placeholder': 'Select a date',
                       'type': 'date'  # <--- IF I REMOVE THIS LINE, THE INITIAL VALUE IS DISPLAYED
                      }),
    }

